﻿using AutoMapper;
using QuanLySinhVien.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuanLySinhVien.Models
{
    public class MappingProfile: Profile
    {
        public MappingProfile()
        {
            //đảo ngược (2 chiều) .ReverseMap()
            CreateMap<SinhVien, DiemViewModel>()
                .ForMember(d => d.MaSV,
                opt => opt.MapFrom(s => s.MaSV)
                )
                .ForMember(d => d.HoTen, opt => opt.MapFrom(s => s.HoTen))
            ;
            CreateMap<MonHoc, DiemViewModel>()
                .ForMember(d => d.MaMon,
                opt => opt.MapFrom(s => s.MaMon)
                )
            ;
            CreateMap<LopHocPhan, DiemViewModel>()
                .ForMember(d => d.MaLHP,
                opt => opt.MapFrom(s => s.MaLHP)
                )
                .ForMember(d => d.NamHoc, opt => opt.MapFrom(s => s.NamHoc))
                .ForMember(d => d.HocKy, opt => opt.MapFrom(s => s.HocKy))
                .ForMember(d => d.DiemGK, opt => opt.MapFrom(s => s.DiemGK))
                .ForMember(d => d.DiemCuoiKy, opt => opt.MapFrom(s => s.DiemCuoiKy))
            ;
        }
    }
}
