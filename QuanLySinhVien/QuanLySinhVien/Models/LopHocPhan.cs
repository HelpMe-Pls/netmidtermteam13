﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace QuanLySinhVien.Models
{
    [Table("LopHocPhan")]
    public class LopHocPhan
    {
        [Key]
        public string MaLHP { get; set; }
        public int NamHoc { get; set; }
        public int HocKy { get; set; }
        public double DiemGK { get; set; }
        public double DiemCuoiKy { get; set; }
        [ForeignKey("MaMon")]
        public string Mon { get; set; }
    }
}
