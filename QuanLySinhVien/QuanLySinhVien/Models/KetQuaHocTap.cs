﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace QuanLySinhVien.Models
{
    [Table("KetQuaHocTap")]
    public class KetQuaHocTap
    {
        [Key]
        public string MaKQHT { get; set; }
        [ForeignKey("MaLHP")]
        public string LopHocPhan { get; set; }
        [ForeignKey("MaMon")]
        public string Mon { get; set; }
        [ForeignKey("MaSV")]
        public string SinhVien { get; set; }
    }
}
