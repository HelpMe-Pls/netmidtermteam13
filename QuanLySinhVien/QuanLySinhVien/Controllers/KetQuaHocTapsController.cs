﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using QuanLySinhVien.Models;

namespace QuanLySinhVien.Controllers
{
    public class KetQuaHocTapsController : Controller
    {
        private readonly QuanLySinhVienContext _context;

        public KetQuaHocTapsController(QuanLySinhVienContext context)
        {
            _context = context;
        }

        // GET: KetQuaHocTaps
        public async Task<IActionResult> Index()
        {
            return View(await _context.KetQuaHocTap.ToListAsync());
        }

        // GET: KetQuaHocTaps/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ketQuaHocTap = await _context.KetQuaHocTap
                .FirstOrDefaultAsync(m => m.MaKQHT == id);
            if (ketQuaHocTap == null)
            {
                return NotFound();
            }

            return View(ketQuaHocTap);
        }

        // GET: KetQuaHocTaps/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: KetQuaHocTaps/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("MaKQHT,LopHocPhan,Mon,SinhVien")] KetQuaHocTap ketQuaHocTap)
        {
            if (ModelState.IsValid)
            {
                _context.Add(ketQuaHocTap);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(ketQuaHocTap);
        }

        // GET: KetQuaHocTaps/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ketQuaHocTap = await _context.KetQuaHocTap.FindAsync(id);
            if (ketQuaHocTap == null)
            {
                return NotFound();
            }
            return View(ketQuaHocTap);
        }

        // POST: KetQuaHocTaps/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("MaKQHT,LopHocPhan,Mon,SinhVien")] KetQuaHocTap ketQuaHocTap)
        {
            if (id != ketQuaHocTap.MaKQHT)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(ketQuaHocTap);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!KetQuaHocTapExists(ketQuaHocTap.MaKQHT))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(ketQuaHocTap);
        }

        // GET: KetQuaHocTaps/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ketQuaHocTap = await _context.KetQuaHocTap
                .FirstOrDefaultAsync(m => m.MaKQHT == id);
            if (ketQuaHocTap == null)
            {
                return NotFound();
            }

            return View(ketQuaHocTap);
        }

        // POST: KetQuaHocTaps/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var ketQuaHocTap = await _context.KetQuaHocTap.FindAsync(id);
            _context.KetQuaHocTap.Remove(ketQuaHocTap);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool KetQuaHocTapExists(string id)
        {
            return _context.KetQuaHocTap.Any(e => e.MaKQHT == id);
        }
    }
}
