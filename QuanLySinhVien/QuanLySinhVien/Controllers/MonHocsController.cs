﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using QuanLySinhVien.Models;
using AutoMapper;
using QuanLySinhVien.ViewModels;

namespace QuanLySinhVien.Controllers
{
    public class MonHocsController : Controller
    {
        private readonly QuanLySinhVienContext _context;
        private readonly IMapper _mapper;

        public MonHocsController(QuanLySinhVienContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: MonHocs
        public async Task<IActionResult> Index()
        {
            return View(await _context.MonHoc.ToListAsync());
        }

        // GET: MonHocs/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var monHoc = await _context.MonHoc
                .FirstOrDefaultAsync(m => m.MaMon == id);
            if (monHoc == null)
            {
                return NotFound();
            }

            return View(monHoc);
        }

        // GET: MonHocs/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: MonHocs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("MaMon,TenMon,SoTinChi")] MonHoc monHoc)
        {
            if (ModelState.IsValid)
            {
                _context.Add(monHoc);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(monHoc);
        }

        // GET: MonHocs/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var monHoc = await _context.MonHoc.FindAsync(id);
            if (monHoc == null)
            {
                return NotFound();
            }
            return View(monHoc);
        }

        // POST: MonHocs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("MaMon,TenMon,SoTinChi")] MonHoc monHoc)
        {
            if (id != monHoc.MaMon)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(monHoc);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!MonHocExists(monHoc.MaMon))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(monHoc);
        }

        // GET: MonHocs/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var monHoc = await _context.MonHoc
                .FirstOrDefaultAsync(m => m.MaMon == id);
            if (monHoc == null)
            {
                return NotFound();
            }

            return View(monHoc);
        }

        // POST: MonHocs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var monHoc = await _context.MonHoc.FindAsync(id);
            _context.MonHoc.Remove(monHoc);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool MonHocExists(string id)
        {
            return _context.MonHoc.Any(e => e.MaMon == id);
        }

        public IActionResult GetListSinhVienByMonHoc()
        {
            return View();
        }

        [HttpPost]
        public IActionResult GetListSinhVienByMonHoc(string TuKhoa)
        {
            var dsSinhVien = _context.KetQuaHocTap.AsQueryable();


            if (!string.IsNullOrEmpty(TuKhoa))
            {
                dsSinhVien = dsSinhVien.Where(p => p.Mon.Contains(TuKhoa)).AsQueryable();
                
            }
            
            var result = _mapper.Map<List<DiemViewModel>>(dsSinhVien.ToList());

            ViewBag.Data = result;
            return View();
        }
    }
}
