﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuanLySinhVien.ViewModels
{
    public class DiemViewModel
    {
        public string MaMon { get; set; }
        public string MaSV { get; set; }
        public string HoTen { get; set; }
        public string MaLHP { get; set; }
        public int NamHoc { get; set; }
        public int HocKy { get; set; }
        public double DiemGK { get; set; }
        public double DiemCuoiKy { get; set; }
    }
}
