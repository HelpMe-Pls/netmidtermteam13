﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using QuanLySinhVien.Models;

namespace QuanLySinhVien.Models
{
    public class QuanLySinhVienContext : DbContext
    {
        public QuanLySinhVienContext (DbContextOptions<QuanLySinhVienContext> options)
            : base(options)
        {
        }

        public DbSet<QuanLySinhVien.Models.SinhVien> SinhVien { get; set; }
        public DbSet<QuanLySinhVien.Models.MonHoc> MonHoc { get; set; }
        public DbSet<QuanLySinhVien.Models.Khoa> Khoa { get; set; }
        public DbSet<QuanLySinhVien.Models.LopHocPhan> LopHocPhan { get; set; }
        public DbSet<QuanLySinhVien.Models.KetQuaHocTap> KetQuaHocTap { get; set; }
    }
}
